$(document).ready(function() {

	
    initFirebaseLib();
    closeSession();
    initHandlers();

});

//Init jQuery componentns handlers
function initHandlers() {
    
   	 
    $('.alert').hide();
    $('.loader').hide();

    
    $( '#loginBtn').click(function() { 
    	$('#loginBtn').hide(); 
    	$('.alert').hide();
    	$('.loader').show();
        loginUser();
    });
}

//close user session
function closeSession(){
	localStorage.setItem("userSession", "");
	firebase.auth().signOut().then(function() {}).catch(function(error) {});

}

//Login user function
function loginUser(){
	let email = $('#nptEmail').val();
	let password = $('#nptPassword').val();

	loginUserFirebase(email,password,loginResponse);
}

//Response for login function
function loginResponse(user,error){
 

	if (error == null && user != null){
		let userJson = JSON.stringify(user);    
		localStorage.setItem("userSession", userJson);
		showMessage(false,"Success user validation");
		goHome();
	}else{
		showMessage(true, error.message);
	}

}



//Show successs error messages
function showMessage(isError, message){

	$('.alert').removeClass('alert-success');
	$('.alert').removeClass('alert-danger');

	var type = 'alert-success';

	if (isError){
		type = 'alert-danger';
	}

	$('#loginBtn').show();
    $('.loader').hide();
    $('.alert').addClass(type);
    $('.alert').show();
    $('.alert').html(message);
}



function goHome(){ 
	setTimeout(function(){ location.href = "home.html" }, 2000);
}
