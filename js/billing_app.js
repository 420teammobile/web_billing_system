
var isLoadingBillingFile = false;
var isFirstTime = true;
var table = null;

var customerPurchaseHistoryList = null;
var customerList = null;


var productPurchasedList = null;

$(document).ready(function() {
 
	initFirebaseLib();
	showUserData(); 
    getBillingList(billingListResponse); 
	loadHandlers();
 
});

 

//Ger Firebse user data
function showUserData(){

	let jsonSession =  localStorage.getItem("userSession");
	let userSession =  JSON.parse(jsonSession);

	if (userSession != null){
		$("#providerEmail").html(userSession.email);
	}else{
		closeSession();
	}
}

//handlers
function loadHandlers() {
   
}
 
 //lister firebase billing list response
 function billingListResponse() {
   let jsonCustomerPurchaseHistory =  localStorage.getItem("customerPurchaseHistory");
   customerPurchaseHistoryList =  JSON.parse(jsonCustomerPurchaseHistory);


      let jsonCustomer =  localStorage.getItem("customer");
   customerList =  JSON.parse(jsonCustomer);

      let jsonProductPurchased =  localStorage.getItem("productPurchased");
   productPurchasedList =  JSON.parse(jsonProductPurchased);
 
	 if(customerPurchaseHistoryList != null && customerList != null && productPurchasedList != null){
	 	reDrawBillingTable();
	 	showUpdatedBilling();
	 }
}

//Crete tabled based in json list
 
 function reDrawBillingTable(){
 

 
 	var tableBody = '';

	for(i=0;i<customerPurchaseHistoryList.length;i++){

		let billing = customerPurchaseHistoryList[i];
		let billingCustomer = parseInt(billing['idCustomer']);
		let billingOrderPurchase = parseInt(billing['orderPurchase']);

		var quantity = 0;  
		var costumerName = ""; 
		var costumerEmail = "";

		for(j=0;j<customerList.length;j++){

			let idCostumer = parseInt(customerList[j].id); 
			if(billingCustomer == idCostumer){
				costumerName =  customerList[j].name;  
				costumerEmail =  customerList[j].user;
			}
		}

		for(j=0;j<productPurchasedList.length;j++){

			let orderPurchase = parseInt(productPurchasedList[j].orderPurchase); 
			if(billingOrderPurchase == orderPurchase){
				quantity = parseInt(productPurchasedList[j].quantity);  
			}
		}

		var tableRow =  "<tr>"+ 
		                    "<td>"+costumerName+"</td>"+
		                    "<td>"+costumerEmail+"</td>"+
		                    "<td>"+quantity+"</td>"+
		                  "</tr>";
  
		 tableBody += tableRow;

	}

	let table = createBillingTable(tableBody);

	$("#tableBilliingContainer").html("");
	$("#tableBilliingContainer").html(table);
	$('#tblBillings').dataTable();
}

 

//Creta html5 tag table
function createBillingTable(tableBody){

 

	let table = "<table class='table' id='tblBillings'>"+
		"<thead>"+
		"<tr>"+
		"<th>Costumer</th>"+
		"<th>Email</th>"+ 
		"<th>Quantity Products</th>"+
		"</tr>"+
		"</thead>"+
		"<tbody id='tblProductsBody'>"+
		tableBody+
		" </tbody>"+
		"</table>";

		return table; 
}

function showUpdatedBilling(){

	if (!isFirstTime){
	
		$.gritter.add({ 
            title: 'Billing is updated', 
            text: 'The billing history has updated'
        });
	}else{
			isFirstTime = false;
	}

	
}

 

 