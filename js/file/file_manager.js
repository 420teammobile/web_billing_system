 

//Capture form action
function createJSONFromCSVObject(csvJSON) {

	//Save CSV file for off-line mode
  localStorage.setItem("uploadedBillingFile", csvJSON);	
  loadListFromJSON();
 
}

function loadListFromJSON(){

  let csvJSON =  localStorage.getItem("uploadedBillingFile");
  console.log("csvJSON--->"+csvJSON);

    //Create object list for data base insertion
   var inventoryList = new Array();
   let jsonArray =  csvJSON.split("\\r\\n");

   for (i = 1; i < jsonArray.length; i++) {
      let row = createRow(jsonArray[i]);
      inventoryList.push(row);
    }

    loadJSONFromCSVResponse(inventoryList);
}



//Creete row object for json parse
function createRow(csvRow){
	 let rowArray = csvRow.split(";");
   let wareHouse = rowArray[2];
   let wareHouseArray = wareHouse.split('\"');
 

	 let row = {
   			'idProduct':rowArray[1],
   			'idWarehouse':wareHouseArray[0],
        'quantity':parseInt(rowArray[0])
   		}
   	return row;
}

 

 

 