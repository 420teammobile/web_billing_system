
var isLoadingBillingFile = false;
var isFirstTime = true;
var table = null;


$(document).ready(function() {
 
	initFirebaseLib();
    getPickerList(pickerListResponse); 
	loadHandlers();
    showUserData(); 

    checkPendinUploadFiles(); 
});

function checkPendinUploadFiles(){
	let csvJSON =  localStorage.getItem("uploadedBillingFile");
	if (csvJSON != null){
		if(csvJSON != ""){ 
			$('#myModal').modal('show'); 
		}
	}  
}

//Ger Firebse user data
function showUserData(){

	let jsonSession =  localStorage.getItem("userSession");
	let userSession =  JSON.parse(jsonSession);

	if (userSession != null){
		$("#providerEmail").html(userSession.email);
	}else{
		closeSession();
	}

	 
}



//Capture form action
function loadHandlers() {

	 $( "#btnResetUploadFile" ).click(function() {
	 		$('#myModal').modal('hide');
	 		loadListFromJSON();
	 }); 

    $( "#csv" ).change(function() { 
  		var reader = new FileReader();
	    reader.onload = function () {
	    	 isLoadingBillingFile = true;
	    	 let json = JSON.stringify(reader.result);  
	         createJSONFromCSVObject(json,loadJSONFromCSVResponse);
	    }; 
	    reader.readAsBinaryString(this.files[0]);
	});   
}

//Load selet tag for creat a inventory
function loadPickerList(){

	let productList = JSON.parse(localStorage.getItem("product"));
	let wareHouseList = JSON.parse(localStorage.getItem("wareHouse"));

	for(i=0;i<productList.length;i++){
		let product = productList[i];
		$("#selectProducts").append("<option value='"+product.id+"'>"+product.name+"</option>");
	}

	for(i=0;i<wareHouseList.length;i++){
		let wareHouse = wareHouseList[i];
		$("#selectWarehouse").append("<option value='"+wareHouse.id+"'>"+wareHouse.name+"</option>");
	}
	
}


function loadJSONFromCSVResponse(inventoryList){
	updateInvetoryByCSVList(inventoryList); 
}

//Response from firebase load picker list data
function pickerListResponse(){
	loadPickerList();
}

function showUpdatedInventoryList(){

	console.log("isFirstTime-->"+isFirstTime);

	if (!isFirstTime){
		$.gritter.add({ 
            title: 'Inventory Updated!', 
            text: 'The inventory was edited. The product list was updated.'
        });
	}else{
		isFirstTime = false;
	}
	
}

function reDrawTable(inventoryList){

	showUpdatedInventoryList();
 
	let productList = JSON.parse(localStorage.getItem("product"));
	let wareHouseList = JSON.parse(localStorage.getItem("wareHouse")); 
	var isProductMinusStock = false;

 
 
 	var tableBody = '';

	 

	for(i=0;i<inventoryList.length;i++){

		let inventory = inventoryList[i];

		var productName = ""; 
		var productPhotoUrl = ""; 
		var warehouseName = ""; 

		for(j=0;j<productList.length;j++){

			let idList = parseInt(productList[j].id);
			let idFind = parseInt(inventory.idProduct);
 

			if(idList == idFind){
				productName = productList[j].name; 
				productPhotoUrl = productList[j].image; 
			}

		}

		for(k=0;k<wareHouseList.length;k++){

			let idList = parseInt(wareHouseList[k].id);
			let idFind = parseInt(inventory.idWarehouse);

			if(idList == idFind){
				warehouseName = wareHouseList[k].name; 
			}

		}

	
		let quantity = parseInt(inventory['quantity']);

		if(quantity < 11){
			isProductMinusStock = true;
		}


		var tableRow =  "<tr>"+
							"<td><p class='centered'><img src='"+productPhotoUrl+"' class='img-circle' width='40' height='40'></p></td>"+
		                    "<td>"+productName+"</td>"+
		                    "<td>"+warehouseName+"</td>"+
		                    "<td>"+quantity+"</td>"+
		                  "</tr>";
  
		 tableBody += tableRow;

	}


	let table = createTable(tableBody);

	$("#tableContainer").html("");
	$("#tableContainer").html(table);
	$('#tblProducts').dataTable();

	if(isProductMinusStock){
		showMinusProductsStock();
	} 
}


function showMinusProductsStock(){
	$.gritter.add({ 
            title: 'Prodcut minus quantity', 
            text: 'There are products with less than 10 units in the inventory.'
        });
}

//Creta html5 tag table
function createTable(tableBody){

 

	let table = "<table class='table' id='tblProducts'>"+
		"<thead>"+
		"<tr>"+
		"<th>Photo</th>"+
		"<th>Product</th>"+
		"<th>Warehouse</th>"+
		"<th>Quantity</th>"+
		"</tr>"+
		"</thead>"+
		"<tbody id='tblProductsBody'>"+
		tableBody+
		" </tbody>"+
		"</table>";

		return table; 
}

 

 