
//Init Firebase library

var userLogged = null;

var isWareHouseLoaded = false;
var isProductLoaded = false;
var isInventoryLoaded = false;

var isProductPurchasedLoaded = false;
var isCustomerLoaded = false;
var isCustomerPurchaseHistoryLoaded = false;

var isFillList = false;
var isFillBillingList = false;
var database;

function initFirebaseLib(){
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();

    database = firebase.database();

    singOutAccounts();
}

function singOutAccounts(){
	firebase.auth().signOut().then(function() {}).catch(function(error) {});
}

//Login user with FirebaseAUTH
function loginUserFirebase(email,password,loginResponse){

	firebase.auth().onAuthStateChanged(function(user) {
  		loginResponse(user,null);
	});

	firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
	  loginResponse(null,error);
	});
}

//Pre load list for create one product
function getPickerList(pickerListResponse){

	var starCountRefInventory = database.ref('Inventory');
	starCountRefInventory.on('value', function(snapshot) {
		localStorage.setItem("inventory", JSON.stringify(snapshot.val()));
		isInventoryLoaded = true;
		if (!isFillList){
			responseForPickerList(pickerListResponse);
		}

		inventoryListIsUpdated();
	});

	var starCountRefProduct = database.ref('Product');
	starCountRefProduct.on('value', function(snapshot) {
		localStorage.setItem("product", JSON.stringify(snapshot.val()));
		 isProductLoaded = true;
	    if (!isFillList){
			responseForPickerList(pickerListResponse);
		}
	});

	var starCountRefWarehouse = database.ref('Warehouse');
	starCountRefWarehouse.on('value', function(snapshot) {
		localStorage.setItem("wareHouse", JSON.stringify(snapshot.val()));
		isWareHouseLoaded = true;
	  	if (!isFillList){
			responseForPickerList(pickerListResponse);
		}
	});
}

function getBillingList(billingResponse){
 

	var starCountRefProductPurchased = database.ref('ProductPurchased');
	starCountRefProductPurchased.on('value', function(snapshot) {
		localStorage.setItem("productPurchased", JSON.stringify(snapshot.val()));
		isProductPurchasedLoaded = true;
		if (!isFillBillingList){
			responseForBilling(billingResponse);
		}
			billingListResponse();
	});

	var starCountRefCustomer = database.ref('Customer');
	starCountRefCustomer.on('value', function(snapshot) {
		localStorage.setItem("customer", JSON.stringify(snapshot.val()));
		isCustomerLoaded = true;
		if (!isFillBillingList){
			responseForBilling(billingResponse);
		}
			billingListResponse();
	});


	var starCountRefCustomerPurchaseHistory = database.ref('CustomerPurchaseHistory');
	starCountRefCustomerPurchaseHistory.on('value', function(snapshot) {
		localStorage.setItem("customerPurchaseHistory", JSON.stringify(snapshot.val()));
		isCustomerPurchaseHistoryLoaded = true;
		if (!isFillBillingList){
			responseForBilling(billingResponse);
		}

		billingListResponse();
	});
}

function responseForBilling(billingResponse){
	if(isProductPurchasedLoaded && isCustomerLoaded && billingResponse){ 
		isFillBillingList = true;
		billingResponse();
	}
}

//Response the hanlder when all list is fill
function responseForPickerList(pickerListResponse){
	if(isWareHouseLoaded && isProductLoaded && isInventoryLoaded){
		isFillList = true;
		pickerListResponse();
	}
}


 

