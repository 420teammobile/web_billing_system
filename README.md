# README #

This project contains the web part of the suppliers who can upload an inventory through a csv file and can see the billing history.

### Main pages ###

* providers_creation.html (Providers creation)
* login.html (Providers login)
* home.html (Invetory update)
* billing.html (Billing list)

### Web Hosting ###

* https://thepartners.com.co/bms420/login.html
* Users:
*    proveedor_1@gmail.com  --->  prov_1_2020 
*    proveedor_2@gmail.com  --->  prov_2_2020 
*    proveedor_3@gmail.com  --->  prov_3_2020 
*    proveedor_4@gmail.com  --->  prov_4_2020 
*    proveedor_5@gmail.com  --->  prov_5_2020 
* Data set:
* https://bitbucket.org/420teammobile/web_billing_system/src/master/data_set/


### Technologies ###

* Javascript
* Firebase
* Dashio Free HTML5 Template (https://templatemag.com/dashio-bootstrap-admin-template/)

### Developers ###

* Jose Daniel Avalos Avalos
* Sebastian Alfredo Panesso Laverde 
 